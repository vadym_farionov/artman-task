-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Гру 12 2019 р., 19:14
-- Версія сервера: 5.7.25
-- Версія PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `basic`
--

-- --------------------------------------------------------

--
-- Структура таблиці `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1576140114),
('m130524_201442_init', 1576142327),
('m190124_110200_add_verification_token_column_to_user_table', 1576142327);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `first_name`, `last_name`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'dfsg', '2gIj3_zdkoB4h2lvyr6ZhHGmdJ7_fqZI', '$2y$13$NebEz2qUMTWozMPT5vkBx.pxujKzBb2Ecwk2IaNcJf9YtXiy.XEnC', NULL, 'gregerg@ege.com', 'ergerg', 'erger', 1, 1576142890, 1576142890, NULL),
(2, 'Vadym', 'wffKftUP7mIqGZJVOagYaxvlYt_LtR26', '$2y$13$fuvUgtJslfjGEjtTKkRdleBfJRXQR96rC//i6HNdmzVAqKkkX1vRC', NULL, 'farionovvadim@gmail.com', 'Vadym', 'Farionov', 1, 1576149008, 1576166612, NULL),
(3, 'Sergii', 'rHo5szZs5RLEmk8iPm0MovxZn9afNqnJ', '$2y$13$0p5EwMfdlW/WMkahsKA48ebGyi.8KLI/71c7F3G78lL5lYy2N.LTm', NULL, 'gregerg@com.ua', 'Sergii', 'George', 1, 1576157197, 1576157197, NULL),
(4, 'ewwegfwe', 'xHwE_ojobcMHKTzw5L7HBjIZfM7j-pBQ', '$2y$13$nz13W74lrshpBqpOP4YCw.wReLOXARm5nMcI38.oIxdR7h9sjesPq', NULL, 'wegw@sgerg.wef', 'Sergii', 'Farionov', 1, 1576157609, 1576157609, NULL),
(5, 'ergerg', '8smDAFYh8kg5AyiCmPy54GI5_rhlYUDr', '$2y$13$Qgd4xxdm6gERJ7ZokBxovOzCQKTIWzWtnrmGGi6iWK9xMkUQNW5qe', NULL, 'sg@rewger.sg', 'Sergii', 'ehheher', 0, 1576157773, 1576162272, NULL);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
