<?php
/**
 * @var $model \app\models\User
*/
 ?>
<h2><?= $model->name?></h2>
<?= \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'first_name',
        'last_name',
        ['label' => 'Status', 'value' => function($model){
    return \app\models\User::getStatusList()[$model->status];
        }],
        'created_at:datetime',
    ],
]) ?>