<?php
/**
 * @var User $user
 */

use app\models\User;
$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive"/>
        </div>
        <div class="col-sm-6 col-md-8">
            <p><strong>Name:</strong> <?= $user->first_name?></p>
            <p><strong>Surname:</strong> <?= $user->last_name?></p>
            <p><strong>Created account:</strong> <?=date("j F, Y, g:i a", $user->created_at);  ?></p>
            <!-- Split button -->
            <div class="btn-group">
                <? $form = \yii\widgets\ActiveForm::begin();?>
                <?= $form->field($user, 'status')->dropDownList(User::getStatusList(),['onchange'=>'this.form.submit()'])?>
                <?php \yii\widgets\ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>