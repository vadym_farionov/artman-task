<?php
/**
 * @var $users \app\models\User;
 */
$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $users,
            'itemOptions'  => ['class' => 'item'],
            'itemView'     => 'preview',
            'summary' => false
        ]) ?>

